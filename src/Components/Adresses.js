import React, { useEffect, useState } from "react";
import { Pen, Trash } from "react-bootstrap-icons";
import data from "../Data";
import { Link } from "react-router-dom";

function Adresses() {
  const [myAdresses, setMyAdresses] = useState([]);
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  useEffect(() => {
    const {user}= JSON.parse(localStorage.getItem("persist:root"));
    const username=JSON.parse(user).username;
    const url = "http://localhost:8080/api/user/" +username;
    const token = localStorage.getItem("access_token");
    const fetchData = async () => {
      try {
        const response = await fetch(url, {
          headers: {
            Authorization: `Bearer ${token}`,
            "content-Type": "application/json",
          },
        });
        const json = await response.json();
        setMyAdresses(json.adresses);
        setLastName(JSON.parse(user).lastName)
        setName(JSON.parse(user).name)
      } catch (error) {
        console.log("error", error);
      }
    };

    fetchData();
  }, []);
  return (
    <>
      <div class="container">
      <div className="row text-center mb-5">
          <h4>Adresses</h4>
        </div>
        <div class="row">
          <div class="col-12">
          <Link to={"/adressForm"}>          
            <button class="col-2 btn btn-primary btn-lg rounded-0 mb-3">
              Ajouter une adresse
            </button>
          </Link>
            {myAdresses && myAdresses.map((adress) => (
              <>
                <div class="card card-lg bg-light mb-8 rounded-0 mb-3">
                  <div class="card-body">
                    <div className="row">
                        <div className="col-6">                    
                          <h6 class="mb-6">
                          {adress.isInvoiceAdress
                            ? "Adresse de livraison"
                            : "Adresse"}
                          </h6>
                        </div>
                      <div className="col-6 text-end">                
                      <button className="btn btn-lg  button-margin" >
                      <Link to={"/adress/"+ adress.id}><Pen size={20} /></Link>
                      </button>
                      <button className="btn btn-lg  button-margin">
                        <Trash size={20} />
                      </button>
                   </div>
                  </div>
                    <div class="text-muted">
                      <div>
                        {name} {lastName}
                      </div>
                      <div>{adress.adressPartOne}</div>
                      <div>{adress.adressPartTwo}</div>
                      <div>
                        {adress.zip} {adress.city}
                      </div>
                    </div>
                  </div>
                </div>
              </>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

export default Adresses;
